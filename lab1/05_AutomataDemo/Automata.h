﻿#ifndef AUTOMATA_H
#define AUTOMATA_H

#include "Currency.h"
#include "MyStr.h"

using namespace std;


class Automata
{
public:
	Automata();
	~Automata();

	void setON();
	void setOFF();	

	void operate();

	void loadMenu(const MyStr &, const Currency &, const int&, const int&, const int&, const int&, const int&, const int& _sugar=0);
	void putCoin();
	void addToCash(const int);
	void printMenu()const;
	void getChoice();
	void choice(const int, const bool);
	void cooking(const int, const bool);
	void finish();


private:
	Currency cash;
	MyStr * menu;
	int menuSize;
	Currency * prices;

	int * water;
	int * coffee;
	int * milk;
	int * cacao;
	int * tea;
	int * sugar;

	int waterFull;
	int coffeeFull;
	int milkFull;
	int cacaoFull;
	int teaFull;
	int sugarFull;

	enum mode {
		off,	//0
		on,		//1
		wait,	//2
		accept,	//3
		check,	//4
		cook	//5
	};
	mode status;

};




#endif