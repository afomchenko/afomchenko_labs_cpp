﻿#ifndef MY_STR_H
#define MY_STR_H
#include <iostream>

using namespace std;


class MyStr
{
	//вывод в поток
	friend ostream & operator<<(ostream& stream, const MyStr & _outStr);
	//извлечь из потока
	friend istream & operator>>(istream& stream, MyStr & _inStr);
	//перегрузка сложения строк
	friend MyStr operator+(const MyStr& _left,const MyStr& _right);
	//перегрузка сравнения
	friend bool operator==(const MyStr&,const MyStr&);
	friend bool operator!=(const MyStr&,const MyStr&);
public:
	//пустая строка длины 256
	MyStr();
	//передача строки
	MyStr(const char *);
	//пустая строка определенной длины
	MyStr(const int);
	//конструктор копирования
	MyStr(const MyStr &);
	//деструктор
	~MyStr();

	//извлечь строку
	char * getStr()const { return string; } 
	//распечатать строку
	void printStr()const { cout << string; } 

	//перегруженые операторы
	MyStr  operator^(const int);
	MyStr& operator=(const MyStr&);
	MyStr& operator+=(const MyStr&);

	char operator[](const int)const;

	//поиск символа
	int searchSym(const char &)const;
	//поиск строки
	int searchWord(const char *)const;

private:
	char * string;

};



#endif