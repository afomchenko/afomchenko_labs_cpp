﻿/*
Разработать класс DateTime для работы с датой/временем. В качестве основы
взять функции и структуры для работы с временем из стандартной библиотеки.
Добавить в класс DateTime следующие методы:
конструктор с тремя числовыми параметрами (день, месяц,год);
конструктор без параметров (объект использует текущую дату);
конструктор копирования (создаём копию другого объекта);
printToday() - для вывода на экран текущей даты в виде строки, с указанием
дня недели и названия месяца;
printYesterday() - для вывода на экран даты вчерашнего дня в виде строки, с
указанием дня недели и названия месяца;
printTomorrow() - для вывода на экран даты завтрашнего дня в виде строки,
с указанием дня недели и названия месяца.
printFuture() - для вывода даты через N дней в будущем;
printPast() - для вывода даты через N дней в прошлом;
printMonth() - для вывода названия текущего месяца;
printWeekDay() - для вывода названия текущего дня недели;
calcDifference() - для расчёта разницы (в днях) между двумя датами
*/

#include "DateTime.h"
#include <ctime>
#include <iostream>

using namespace std;

int main()
{
	DateTime date(21,3,2014);
	cout << "program written:\t";
	date.printToday();

	DateTime date2;
	cout << "today:\t\t";
	date2.printToday();
	cout << "year future:\t";
	date2.printFuture(365);
	cout << "tomorrow:\t";
	date2.printTomorrow();

	cout << "year past:\t";
	date2.printPast(365);
	cout << "yesterday:\t";
	date2.printYesterday();


	cout << "difference (date1 and date2): ";
	date.printDiff(date2);

	return 0;
}