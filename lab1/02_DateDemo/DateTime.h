﻿#ifndef DATETIME_H
#define DATETIME_H


using namespace std;

class DateTime
{
public:
	DateTime();
	DateTime(const int, const int, const int);
	DateTime(const DateTime &);

	void printToday()const;
	void printMonth()const;
	void printWeekDay()const;
	DateTime getFuture(const int)const;
	void printFuture(const int)const;
	void printFuture()const;
	void printTomorrow()const;
	DateTime getPast(const int)const;
	void printPast(const int)const;
	void printPast()const;
	void printYesterday()const;
	int  getWeekDay()const;
	char * getWeekDayText()const;
	char * getMonthText()const;
	
	int getDiff(const DateTime &)const;
	void printDiff(const DateTime &)const;

private:
	int day;
	int mon;
	int yr;
};



#endif