﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include "student.h"
#include "group.h"

static const int sizeGroup = 64;

using namespace std;


group::group(const char * _name) : studCount(0)
{
	name = new char[strlen(_name) + 1];
	strcpy(name, _name);
	stud = new student[sizeGroup];
}

group::~group()
{
	cout << "delete group" << endl;
	delete[] name;
	delete[] stud;
}

void group::addStudent(student * _stud)
{
	if (studCount < sizeGroup)
	{
		stud[studCount] = *_stud;
		studCount++;
	}
	else cout << "too many students"<<endl;
}

void group::removeStudent(student * _stud)
{
	int studNo, studNum;
	for (studNo = 0; studNo < sizeGroup; ++studNo)
	{
		if (stud[studNo].getName() == _stud->getName() && stud[studNo].getFirstname() == _stud->getFirstname()) break;
	}
	for (studNum = studNo; studNum < sizeGroup-2; ++studNum)
	{
		stud[studNum] = stud[studNum + 1];
	}
	studCount--;
}

void group::printGroup()const
{
	cout << "group" << name << endl;
	cout << "---------------" << endl;
	for (int studNo = 0; studNo < studCount; ++studNo)
	{
		stud[studNo].printStudent();
	}
	cout << "---------------" << endl;
}

double group::avGroup()const
{
	double gradeSum=0;
	for (int studNo = 0; studNo < studCount; ++studNo)
	{
		gradeSum+=stud[studNo].avGrade();
	}
	return gradeSum / studCount;
}