﻿#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>

using namespace std;

class student
{
	friend class group;
public:
	student();
	student(const char *, const char *);
	~student();
	char * getFirstname()const;
	char * getName()const;
	int getGrade(const int)const;
	void printStudent()const;
	bool addGrade(const int);
	double avGrade()const;

private:
	char * firstname;
	char * lastname;
	int * grade;
};

#endif

