﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include "student.h"

using namespace std;


student::student()
{
	firstname = NULL;
	lastname = NULL;
	grade = NULL;
	//cout << "student NULL"<< endl;
}

student::student(const char * _firstname, const char * _lastname)
{
	firstname = new char[strlen(_firstname)+1];
	strcpy(firstname, _firstname);
	lastname = new char[strlen(_lastname)+1];
	strcpy(lastname, _lastname);
	grade = new int[32];
	//cout << "student " << firstname << lastname << endl;
}

student::~student()
{
	//cout << "delete"<< endl;
	delete [] firstname;
	delete [] lastname;
	delete[] grade;
}

char * student::getFirstname()const
{
	return firstname;
}

char * student::getName()const
{
	return lastname;
}

int student::getGrade(const int gradeNo)const
{
	return grade[gradeNo];
}

void student::printStudent()const
{
	cout << getFirstname() << ' ' << getName() << ": ";

	int gradeNo=0;
	while (grade[gradeNo] <= 5 && grade[gradeNo] > 1)
	{
		cout << getGrade(gradeNo) << ' ';
		gradeNo++;
	}
	cout << "average:" << avGrade() <<  endl;
}

bool student::addGrade(const int _grade)
{
	if (_grade <= 5 && _grade > 1)
	{
	
		for (int i = 0; i < 32; ++i)
		{
			if (grade[i] > 5 || grade[i]<=1) { grade[i] = _grade; return 1; }
		}
		cout << "not enough memory for grade" << endl;

	}
	return 0;
}

double student::avGrade()const
{
	int gradeNo = 0, gradeSum=0;
	while (grade[gradeNo] <= 5 && grade[gradeNo] > 1)
	{
		gradeSum += grade[gradeNo];
		gradeNo++;
	}
	return static_cast<double>(gradeSum) / gradeNo;
}