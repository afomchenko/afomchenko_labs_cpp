﻿#include <iostream>
#include <cstring>
#include "student.h"
#include "group.h"

using namespace std;


int main()
{
	//создаем студентов
	student *a = new student("Ivan", "Ivanov");
	student *b = new student("Petr", "Petrov");
	a->addGrade(5);
	a->addGrade(4);
	a->addGrade(3);
	a->addGrade(3);
	a->addGrade(4);
	b->addGrade(3);
	b->addGrade(3);
	b->addGrade(4);

	//создаем группу
	group A("A");
	//добавляем студентов
	A.addStudent(a);
	A.addStudent(b);
	//выводим список группы и средний балл
	A.printGroup();
	cout << "average:" << A.avGroup() << endl;
	//удаляем студента
	A.removeStudent(a);
	cout << "after delete stud2" << endl;
	b->addGrade(5);
	A.printGroup();
	cout <<"average:"<< A.avGroup() << endl;
	a->printStudent();

	return 0;
}