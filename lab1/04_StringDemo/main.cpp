﻿/*
Разработать класс MyStr для хранения символов строки. Массив символов
должен быть динамический. Предусмотреть следующий набор методов:
конструктор с параметром в виде строковой константы (“…”),
конструктор по-умолчанию (пустые скобки),
конструктор копирования,
операция + для склеивания двух строк,
операция = (присвоение строк),
операция += для склеивания двух строк,
операция [] для доступа к отдельным символам строки,
операция проверки на равенства ==,
операция проверки на неравенство !=,
операция возведения в степень строки ^ (повторение строки заданное
количество раз),
операция вывода строки в поток >>,
операция ввода строки из потока <<,
деструктор,
метод печати строки на экране,
метод поиска символа в строке,
метод поиска подстроки в строке.
*/

#include <iostream>

#include "MyStr.h"

using namespace std;

int main()
{


	MyStr a("Hello, ");
	MyStr b("World!");

	a.printStr();
	cout << endl;

	cout << "Hello, + World! = ";
	MyStr c = a + b;
	c.printStr();
	cout << endl;

	cout << "Hello, ^ 3 = ";
	MyStr d = a ^ 3;
	d.printStr();
	cout << endl;

	cout << "Hello, Hello, Hello, += World! : ";
	d += b;
	d.printStr();
	cout << endl;

	cout << "MyStr 'Hello, Hello, Hello,' = 'Goodbye' : ";
	d = "Goodbye";
	d.printStr();
	cout << endl;

	cout << "Goodbye' + 'cruel world...' : ";
	cout << d + " cruel world..." << endl;


	cout << "Hello, == World! : ";
	cout << (a == b? "true" : "false") << endl;

	cout << "Hello, == Hello, : ";
	cout << (a == a ? "true" : "false") << endl;

	cout << "Hello, != Hello, : ";
	cout << (a != a ? "true" : "false") << endl;

	cout << "first char from Hello, : ";
	cout << a[0] << endl;

	cout << "reloaded << : ";
	cout << a << endl;

	cout << "reloaded >> : ";
	MyStr f;
	cin >> f;
	cout << f << endl;

	cout << "found " << a.searchSym('l') << " 'l' in " << a << endl;
	cout << "found " << a.searchWord("ell") << " 'ell' in " << a << endl;

	return 0;
}