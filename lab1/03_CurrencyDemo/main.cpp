﻿/*
Разработать класс Currency для хранения денежной суммы в виде рублей и
копеек.
В класс Currency включить следующие методы:
=>конструктор>с>параметрами>(рубли,>копейки)>
=>конструктор>по>умолчанию>(с>пустыми>скобками)>
=>конструктор>копирования>
=>печать>суммы>
=>операция>сложения>двух>сумм>+>
=>операция>вычитания>двух>сумм>=>
=>операция>присвоения>двух>сумм>=>
=>операция>сложения>с>присвоением>+=>
=>операция>вычитания>с>присвоением>==>
=>операция>равенства>==>
=>операция>неравенства>!=>
=>функции>доступа>к>рублям>и>копейкам>
=>функции>перевода>в>доллар>и>евро>по>текущему>курсу>(устанавливается>отдельным>методом)>
=>операции>для>работы>с>потоками>(>>,<<)>
*/


#include <iostream>
#include "Currency.h"

using namespace std;

int main()
{
	Currency sum0;
	Currency sum1(10,20);
	Currency sum2(5, 10);

	cout << "sum1: ";
	sum1.printCurrency();
	cout << endl;
	cout << "sum2: ";
	sum2.printCurrency();
	cout << endl;

	cout << "operator+  : ";
	sum0 = sum1 + sum2;
	sum0.printCurrency();
	cout << endl;

	cout << "operator-  : ";
	sum0 = sum1 - sum2;
	sum0.printCurrency();
	cout << endl;

	cout << "operator+= : ";
	sum0 += sum2;
	sum0.printCurrency();
	cout << endl;

	cout << "operator== : ";
	cout << (sum1 == sum2) << endl;

	cout << "exchange USD : ";
	cout << sum1.GetUSD() << " USD" << endl;

	cout << "exchange EUR : ";
	cout << sum1.GetEUR() << " EUR" << endl;

	cout << "operator<< : ";
	cout << sum1  << endl;

	cout << "operator>> : ";
	Currency sum3;
	cin >> sum3;
	cout << sum3 << endl;

	return 0;
}