#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QXmlStreamReader>
#include <QMap>
#include <vector>
#include "student.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void studOutput(Student&);

    std::vector <Student> StudArray;
    std::vector<Student>::iterator studIter;

private slots:

    void on_forward_clicked();

    void on_backward_clicked();

    void on_saveButton_clicked();

    void on_nameBox_editingFinished();

    void on_addNewButton_clicked();

    void on_surnameBox_editingFinished();

    void on_dateBirth_editingFinished();

    void on_specBox_editingFinished();

    void on_gradeBox_editingFinished();


    void on_addPhotoButton_clicked();

private:
    Ui::MainWindow *ui;

    void imageShow(const QString & fileName);
};


#endif // MAINWINDOW_H
