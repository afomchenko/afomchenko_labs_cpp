
#include <QtXml>
#include <vector>
#include "main.h"
#include "mainwindow.h"
#include "student.h"


class MainWindow;
class Student;

QDomElement makeElement( QDomDocument& domDoc,
                         const QString& strName,
                         const QString& strAttr = QString(),
                         const QString& strText = QString()
        )
{
    QDomElement domElement = domDoc.createElement(strName);
    if (!strAttr.isEmpty()) {
        QDomAttr domAttr = domDoc.createAttribute("id");
        domAttr.setValue(strAttr);
        domElement.setAttributeNode(domAttr);
    }
    if (!strText.isEmpty()) {
        QDomText domText = domDoc.createTextNode(strText);
        domElement.appendChild(domText);
    }
    return domElement;
}


QDomElement contact( QDomDocument& domDoc, Student stud)
{
    static int nNumber = 1;
    QDomElement domElement = makeElement(domDoc,
                                         "student",
                                         QString().setNum(nNumber)
                                         );
    domElement.setAttribute("name", stud.name);
    domElement.setAttribute("surname", stud.surname);
    domElement.setAttribute("birth", stud.birthdate.toString("dd.MM.yyyy"));
    domElement.setAttribute("spec", stud.spec);
    domElement.setAttribute("grade", QString::number(stud.grade));
    domElement.setAttribute("photo", stud.image);
    //domElement.appendChild(makeElement(domDoc, "name", "", strName));
    nNumber++;
    return domElement;
}

void exportStudXML(std::vector <Student> & StudArray)
{
    std::vector<Student>::iterator studIterExport;

    studIterExport=StudArray.begin();


    QDomDocument doc;
    QDomNode xmlNode = doc.createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\"");
    doc.insertBefore(xmlNode, doc.firstChildElement());

    QDomElement domElement = doc.createElement("students");
    doc.appendChild(domElement);

    while(studIterExport!=StudArray.end())
    {
    QDomElement contact1 = contact(doc, *studIterExport);
    domElement.appendChild(contact1);
    studIterExport++;
    }

    QFile file("../studentsDB.xml");
    if(file.open(QIODevice::WriteOnly)) {
        QTextStream outXML(&file);
        outXML.setCodec("UTF-8");
        outXML << doc.toString();
        file.close();
    }
}



