#ifndef STUDENT_H
#define STUDENT_H

#include <QString>
#include <QDate>

class Student
{
public:
    Student(const int);

    //static int countStud;
    int id;
    QString name;
    QString surname;
    QDate birthdate;
    float grade;
    QString image;

    enum lang{
        none,   //0
        Cpp,	//1
        Java,	//2
        Python,	//3
        PHP,	//4
        Lisp	//5
    };
    QString spec;
};

#endif // STUDENT_H
