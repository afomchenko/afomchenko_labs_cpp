#include "student.h"
#include <QDate>

Student::Student(const int _id)
{

    static int countStud=0;
    id=countStud++;
    name="";
    surname="";
    birthdate=QDate::fromString("01.01.1970","dd.MM.yyyy");
    grade=0;
    image="empty.jpg";
    spec="";
}
