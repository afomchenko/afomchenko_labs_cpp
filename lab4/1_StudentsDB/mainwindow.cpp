#include "main.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "student.h"
#include <vector>
#include <QDate>
#include <QtGui>
#include <QFileDialog>

using namespace std;

MainWindow::~MainWindow()
{
    delete ui;
}




void scanXMLstudent(QString XMLpath, vector <Student> & StudArray)
{
    QFile * file=new QFile(XMLpath);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox mess;
        mess.setText("file error");
        mess.exec();
        return;
    }

    QXmlStreamReader xml(file);
    QXmlStreamReader::TokenType token;

    int studCount=0;

    while(!xml.atEnd() && !xml.hasError())
    {
        token=xml.readNext();
        if(token==QXmlStreamReader::StartDocument)
            continue;
        if(token==QXmlStreamReader::StartElement)
        {
            if(xml.name()=="students")
                continue;
            if(xml.name()=="student")
            {

                StudArray.push_back(studCount);

                QXmlStreamAttributes attrs=xml.attributes();
                if(attrs.hasAttribute("name"))
                    StudArray[studCount].name=attrs.value("name").toString();
                if(attrs.hasAttribute("surname"))
                    StudArray[studCount].surname=attrs.value("surname").toString();
                if(attrs.hasAttribute("birth"))
                    StudArray[studCount].birthdate=QDate::fromString(attrs.value("birth").toString(),"dd.MM.yyyy");
                if(attrs.hasAttribute("spec"))
                    StudArray[studCount].spec=attrs.value("spec").toString();
                if(attrs.hasAttribute("grade"))
                    StudArray[studCount].grade=attrs.value("grade").toFloat();
                if(attrs.hasAttribute("photo"))
                    StudArray[studCount].image=attrs.value("photo").toString();

                studCount++;
            }
        }
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scanXMLstudent("/home/anthon/prog/cpp/qt/studentsDB.xml", StudArray);

    studIter = StudArray.begin();

    studOutput(*studIter);
}



void MainWindow::on_forward_clicked()
{
   if(studIter<StudArray.end()-1)
   studOutput(*(++studIter));
}

void MainWindow::on_backward_clicked()
{
    if(studIter>StudArray.begin())
    studOutput(*(--studIter));
}

void MainWindow::on_saveButton_clicked()
{
    exportStudXML(StudArray);
    ui->saveButton->setEnabled(false);
}

void MainWindow::studOutput(Student & currStudent)
{
    ui->nameBox->setText(currStudent.name);
    ui->surnameBox->setText(currStudent.surname);
    ui->dateBirth->setDate(currStudent.birthdate);
    //ui->bithBox->setText(QString::number(currStudent.birthdate));
    ui->specBox->setText(currStudent.spec);
    ui->gradeBox->setText(QString::number(currStudent.grade));
    imageShow("../"+currStudent.image);
    if (currStudent.image=="empty.jpg") ui->addPhotoButton->setEnabled(true);
    else ui->addPhotoButton->setEnabled(false);
}

void MainWindow::on_nameBox_editingFinished()
{
    studIter->name=ui->nameBox->text();
}

void MainWindow::on_addNewButton_clicked()
{
    StudArray.push_back(1);
    studIter = StudArray.end();
    studOutput(*(--studIter));
    ui->saveButton->setEnabled(true);
}

void MainWindow::on_surnameBox_editingFinished()
{
    studIter->surname=ui->surnameBox->text();
    ui->saveButton->setEnabled(true);
}

void MainWindow::on_dateBirth_editingFinished()
{
    studIter->birthdate=ui->dateBirth->date();
    ui->saveButton->setEnabled(true);
}

void MainWindow::on_specBox_editingFinished()
{
    studIter->spec=ui->specBox->text();
    ui->saveButton->setEnabled(true);
}

void MainWindow::on_gradeBox_editingFinished()
{
    studIter->grade=ui->gradeBox->text().toFloat();
    ui->saveButton->setEnabled(true);
}

void MainWindow::imageShow(const QString & fileName)
{
    QPixmap pixmap(fileName);
    ui->photoBox->setPixmap(pixmap);
    ui->photoBox->setMask(pixmap.mask());

    ui->photoBox->show();
}

void MainWindow::on_addPhotoButton_clicked()
{
    QString imgPath=QFileDialog::getOpenFileName();
    QFile sourceImg (imgPath);
    QFile destImg (QString("../")+QString::number(studIter->id+1)+".jpg");
    try
    {
        sourceImg.open(QFile::ReadOnly);
        destImg.open(QFile::WriteOnly);
        destImg.write(sourceImg.readAll());
    }
    catch(...)
    {
        sourceImg.close(); destImg.close();
    }
    sourceImg.close(); destImg.close();
    studIter->image=QString::number(studIter->id+1)+".jpg";
    studOutput(*studIter);
    ui->saveButton->setEnabled(true);
}
