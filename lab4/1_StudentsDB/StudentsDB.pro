#-------------------------------------------------
#
# Project created by QtCreator 2014-04-17T18:41:37
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StudentsDB
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    student.cpp \
    exportXML.cpp

HEADERS  += mainwindow.h \
    student.h \
    main.h

FORMS    += mainwindow.ui
