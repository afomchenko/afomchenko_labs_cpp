#ifndef MAZETHREAD_H
#define MAZETHREAD_H

#include <QThread>
#include "mainwindow.h"

class mazeThread : public QThread
{
    friend class MainWindow;

    Q_OBJECT
public:
    explicit mazeThread(QObject *parent = 0);
    void run();
    bool Stop;

    void moveMan (std::vector < std::vector <int> > & maze, int & x, int & y, int & pos);

signals:
    void posChanged(int x, int y, int i);

public slots:

private:
    int x;
    int y;
    int pos;
    std::vector < std::vector <int> > * mazeptr;

};

#endif // MAZETHREAD_H
