#include "mazethread.h"
#include <QtCore>
#include <QThread>
#include <vector>

mazeThread::mazeThread(QObject *parent) :
    QThread(parent)
{
}

void mazeThread::run()
{
    int stepsCount=0;
    QMutex mazeMutex;
    std::vector < std::vector <int> > & maze = *mazeptr;


    while(1)
    {
        if(this->Stop)break;

        mazeMutex.lock();

        //матрица перемещений
        int dirMove[4][3] = { { y, x - 1, maze[y][x-1] }, { y - 1, x, maze[y-1][x]}, { y, x + 1, maze[y][x+1] }, { y + 1, x, maze[y+1][x] } };

        //ищем клетку с минимальным весом
        int dirMin= 9999;
        for (int i = 0; i < 4; i++)
            if (dirMove[i][2] < 100 && dirMove[i][2] < dirMin) dirMin = dirMove[i][2];

        //перемещаем курсор
        qsrand(QDateTime::currentMSecsSinceEpoch());
        int k;
        while (1)
        {
            k = qrand() % 4;
            if (dirMove[k][2] <= dirMin)
            {
                maze[y][x] = pos + 1;

                y = dirMove[k][0]; x = dirMove[k][1];
                pos = dirMove[k][2];



                break;
            }
        }

        mazeMutex.unlock();

        //увеличиваем счетчик шагов
        stepsCount++;

        //сигнал для перерисовки
        emit posChanged(x,y,stepsCount);
        this->msleep(10);
    }

}

