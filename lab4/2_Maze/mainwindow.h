#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QGraphicsScene>
#include <QVector>
#include <QFile>
#include "mazethread.h"

class mazeThread;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    friend class mazeThread;
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    typedef std::vector < std::vector <int> > mazeArray;

private slots:

    void on_actStart_triggered();
    void on_actLoad_triggered();
    void onPosChanged(int _x, int _y, int i);

    void on_actStop_triggered();

    void on_actClear_triggered();

private:
    mazeThread *mazeThreadCurrent;
    Ui::MainWindow *ui;
    QGraphicsScene *scene;

    QGraphicsEllipseItem *man;

    QString mazeMapPath;
    int mazeWidth;
    int mazeHeight;



    mazeArray maze;

    int x;
    int y;
    int pos;

    void mazeLoad();
    void printMaze();
    void moveMan (int & x, int & y, int & pos);
};

#endif // MAINWINDOW_H
