#-------------------------------------------------
#
# Project created by QtCreator 2014-05-13T14:25:05
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2_Maze
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mazethread.cpp

HEADERS  += mainwindow.h \
    mazethread.h

FORMS    += mainwindow.ui

RESOURCES += \
    img.qrc
