#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QGraphicsEllipseItem>
#include <QDateTime>
#include <QFileDialog>
#include <QBrush>
#include <QImage>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mazeThreadCurrent = new mazeThread(this);

    scene = new QGraphicsScene(this);
    ui->mazeView->setScene(scene);
    connect(mazeThreadCurrent, SIGNAL(posChanged(int,int,int)), this, SLOT(onPosChanged(int,int, int)));
    mazeWidth=0;
    mazeHeight=0;

    ui->statusBar->showMessage("для начала загрузите карту");
}

MainWindow::~MainWindow()
{
    delete ui;
}

//загрузка лабиринта
void MainWindow::mazeLoad()
{
    QFile mazeMapFile (mazeMapPath);
    try
    {

        if(mazeWidth)
        {
            for(int row=0; row<=mazeHeight; ++row)
            {
                maze[row].clear();
            }
            maze.clear();
        }


        mazeWidth=1;
        mazeHeight=1;


        mazeMapFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&mazeMapFile);

        in >> mazeWidth;
        in >> mazeHeight;

        this->resize(mazeWidth*10, mazeHeight*10+50);
        ui->mazeView->setGeometry(0,0,mazeWidth*10, mazeHeight*10+3);


        ui->mazeView->sceneRect().setRect(0,0,mazeWidth*10, mazeHeight*10);

        char c;

        std::vector<int> tmpVec;

        for(int i=0; i<=mazeHeight; ++i)
        {
            mazeMapFile.seek(1);
            for(int j=0; j<=mazeWidth; ++j)
            {
                in >> c;
                if (c=='#')
                {
                    tmpVec.push_back(9999);
                }
                else tmpVec.push_back(0);
            }
            maze.push_back(tmpVec);
            tmpVec.clear();
        }
    }
    catch(...)
    {
        mazeMapFile.close();
    }
    mazeMapFile.close();


    printMaze();

}

//вывод лабиринта
void MainWindow::printMaze()
{


    QBrush manTex(QPixmap(":/textures/man"));
    QBrush floorTex(QPixmap(":/textures/grass"));
    QBrush wallTex(QPixmap(":/textures/stone"));
    QPen wallPen(wallTex,0);
    QPen grassPen(floorTex,0);

    for(int i=0; i<mazeHeight; ++i)
    {
        for(int j=0; j<=mazeWidth; ++j)
        {
            if (maze[i][j]==9999)
            {
                scene->addRect((j-1)*10,i*10,10,10,wallPen,wallTex);
            }
            else scene->addRect((j-1)*10,i*10,10,10,grassPen,floorTex);
        }
    }
    man=scene->addEllipse(0,0,15,15,grassPen,manTex);
    man->setFlag(QGraphicsItem::ItemIsMovable);
}

//запуск поиска
void MainWindow::on_actStart_triggered()
{

    //исходная позиция по курсору
    QPointF startPos = man->pos();
    int x=(static_cast <int> (startPos.rx()))/10;
    int y=(static_cast <int> (startPos.ry()))/10;

    if(x>=mazeWidth) x--;
    else if (x<=0) x++;
    if(y>=mazeHeight) y--;
    else if (y<=0) y++;

    int pos=0;

    //загрузка параметров в поток

    mazeThreadCurrent->x=x;
    mazeThreadCurrent->y=y;
    mazeThreadCurrent->pos=pos;
    mazeThreadCurrent->mazeptr=&maze;

    mazeThreadCurrent->Stop=false;
    mazeThreadCurrent->start();



}

//перерисовка
void MainWindow::onPosChanged(int _x, int _y, int i)
{
    QBrush dirTex;
    if(y<_y)
    dirTex.setTexture(QPixmap(":/textures/down"));
    else if (y>_y)
    dirTex.setTexture(QPixmap(":/textures/up"));
    else if (x<_x)
    dirTex.setTexture(QPixmap(":/textures/right"));
    else
    dirTex.setTexture(QPixmap(":/textures/left"));
    x=_x;
    y=_y;
    QPen dirPen(dirTex,0);
    scene->addRect((x-1)*10,y*10,10,10,dirPen,dirTex);
    man->setPos((qreal)(x-1)*10, (qreal)y*10);
    man->setZValue(10);
    ui->statusBar->showMessage("поиск выхода...");



    if (y < 0 || x < 0 || x > mazeWidth || y > mazeHeight-1)
    {
        ui->statusBar->showMessage("выход найден за "+QString::number(i)+" ходов.");
        mazeThreadCurrent->Stop=true;


        for(int i=0; i<mazeHeight; ++i)
        {
            for(int j=0; j<=mazeWidth; ++j)
            {
                if (maze[i][j]==0)
                {
                    maze[i][j]=1000;
                }
                else if  (maze[i][j]<1000) maze[i][j]=0;
            }
        }

    }



}


void MainWindow::on_actLoad_triggered()
{
    mazeMapPath=QFileDialog::getOpenFileName();
    if(mazeMapPath!="")
    {
    scene->clear();
    mazeLoad();
    }
    ui->statusBar->showMessage("загрузка лабиритнта...");


    ui->statusBar->showMessage("для запуска перетащите кружок и выберите \"старт\"");
}

void MainWindow::on_actStop_triggered()
{
    ui->statusBar->showMessage("поиск отменен");
    mazeThreadCurrent->Stop=true;
}

void MainWindow::on_actClear_triggered()
{
    printMaze();
}
