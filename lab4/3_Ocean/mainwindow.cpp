#include "mainwindow.h"
#include "mapredrawthread.h"
#include "ui_mainwindow.h"
#include "sea.h"
#include "prey.h"
#include "predator.h"
#include <typeinfo>
#include <QtCore>
#include <QTime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qsrand(QDateTime::currentMSecsSinceEpoch());

    //океан
    ocean = new OceanMap();

    //потоки
    mapredrawThread = new MapReDrawThread();
    preyThread = new PreyThread;
    predThread = new PreyThread;


    //установка сцены
    scene = new QGraphicsScene(this);
    ui->oceanMapView->setScene(scene);
    scene->addRect(0,0,1200,650);


    //добавляем добычу
    for(int pr=0;pr<6000;++pr)
        ocean->addPrey(qrand()%ocean->width, qrand()%ocean->height);

    //добавляем пару хищников
    ocean->addPredator(70,30);
    ocean->addPredator(100,60);
    ocean->predVector[0].satiety=1000;
    ocean->predVector[0].fertility=110;

    //сервер статистики
    myServer= new MyTcpServer;
    myServer->ocean=ocean;

    redraw();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
    delete preyThread;
    delete predThread;
    delete ocean;
    delete mapredrawThread;
    delete timer;
}

void MainWindow::redraw()
{

    const int PIX_SIZE = 5; //размер клетки

    scene->clear();scene->addRect(0,0,1200,650);
    QBrush greyBrush(Qt::darkGray);
    QBrush greenBrush(Qt::yellow);
    QBrush redBrush(Qt::darkRed);
    QPen greyPen(Qt::darkGray);
    QPen greenPen(Qt::yellow);
    QPen redPen(Qt::darkRed);

    //если typeid соответствует нужному рисуем клетку

    for(int i=0; i<ocean->width; ++i)
    {
        for(int j=0; j< ocean->height; ++j)
        {
            if(ocean->mapVector[i][j].object!=NULL && typeid(*(ocean->mapVector[i][j].object))==typeid(Prey))
            {
                scene->addRect(i*PIX_SIZE-1, j*PIX_SIZE-1, PIX_SIZE, PIX_SIZE, greenPen, greenBrush);
            }
            else if(ocean->mapVector[i][j].object!=NULL && typeid(*(ocean->mapVector[i][j].object))==typeid(Predator))
            {
                scene->addRect(i*PIX_SIZE-1, j*PIX_SIZE-1, PIX_SIZE, PIX_SIZE, redPen, redBrush);

            }
           /* else if(ocean->mapVector[i][j].object!=NULL && typeid(*(ocean->mapVector[i][j].object))==typeid(Rock))
            {
                scene->addRect(i*5-1, j*5-1, 5, 5, greyPen, greyBrush);

            }*/
            else scene->addRect(i*PIX_SIZE-1, j*PIX_SIZE-1, PIX_SIZE, PIX_SIZE, QPen(( QColor(50,160+1*ocean->mapVector[i][j].food,200))), QBrush ( QColor(50,160+1*ocean->mapVector[i][j].food,200)) );;
        }
    }

    //счетчик в статусбаре
    ui->statusBar->showMessage(QString::number(ocean->preyVector.size())+"   "+QString::number(ocean->predVector.size()));
}



void MainWindow::on_actStart_triggered()
{
    //запуск потока добычи
    preyThread->ocean = ocean;
    preyThread->Stop=false;
    preyThread->mode=1;
    preyThread->start();

    //запуск потока хищников
    predThread->ocean = ocean;
    predThread->Stop=false;
    predThread->mode=2;
    predThread->start();

    //запуск потока перерисовки
    mapredrawThread->ocean=ocean;
    mapredrawThread->statbar=ui->statusBar;
    mapredrawThread->scene=scene;
    mapredrawThread->start();


}

void MainWindow::on_actRedraw_triggered()
{
    redraw();
}

void MainWindow::onUpdate()
{
    redraw();
}

void MainWindow::on_actStop_triggered()
{
    preyThread->Stop=true;
    predThread->Stop=true;
    mapredrawThread->Stop=true;
}

void MainWindow::on_zoomin_clicked()
{
    ui->oceanMapView->scale(1.1,1.1);
}

void MainWindow::on_zoomout_clicked()
{
    ui->oceanMapView->scale(0.909,0.909);
}
