#include "oceanmap.h"
#include "predator.h"
#include <QtCore>

OceanMap::OceanMap()
{
    width=240;
    height=130;

    std::vector <Cell> tempVec;

    qsrand(QDateTime::currentMSecsSinceEpoch());
    //qsrand(551);

    for(int i=0; i< width+10; ++i)
    {
        for(int j=0; j< height+10; ++j)
        {
            Cell newCell(qrand()%60+11);

            tempVec.push_back(newCell);

        }
        mapVector.push_back(tempVec);
        tempVec.clear();
    }

}

//добавить добычу
void OceanMap::addPrey(const int _x,const  int _y)
{
    if(mapVector[_x][_y].object==NULL)
    {
        preyVector.push_back(Prey(_x, _y, this));
    }
}

//добавить хищника
void OceanMap::addPredator(const int _x,const  int _y)
{
    if(mapVector[_x][_y].object==NULL)
    {
        predVector.push_back(Predator(_x, _y, this));
    }
}

/*
void OceanMap::addRock(const int _x,const  int _y)
{
    if(mapVector[_x][_y].object==NULL)
    {
        mapVector[_x][_y].object==&rock;

    }
}
*/
