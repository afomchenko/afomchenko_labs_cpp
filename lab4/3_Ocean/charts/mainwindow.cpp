#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include "nightcharts.h"
#include <QTime>
#include <QMessageBox>
class Nightcharts;

int ikkk=1;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent * e)
{



    statistics=socket.doConnect();
    //ui->statLabel->setText(QString::number(statistics.preyCount)+", "+QString::number(statistics.predatorCount));

    if(statistics.predatorCount>0 || statistics.preyCount>0)
    {

        QWidget::paintEvent(e);
        QPainter painter;
        QFont font;
        painter.begin(this);
        Nightcharts PieChart;
        PieChart.setType(Nightcharts::Pie);//{Histogramm,Pie,DPie};
        PieChart.setLegendType(Nightcharts::Vertical);//{Round,Vertical}
        PieChart.setCords(30,60,this->width()/2,this->height()/1.7);
        PieChart.addPiece("Prey",Qt::green,static_cast<double>(statistics.preyCount)/(statistics.preyCount+statistics.predatorCount)*100);
        PieChart.addPiece("Predator",Qt::red,static_cast<double>(statistics.predatorCount)/(statistics.preyCount+statistics.predatorCount)*100);
        ikkk++;

        PieChart.draw(&painter);
        PieChart.drawLegend(&painter);



    }
}
