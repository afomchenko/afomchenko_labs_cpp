#ifndef MYTCPSOCKET_H
#define MYTCPSOCKET_H

#include <QObject>
#include <QDebug>
#include <QTcpSocket>
#include<QDataStream>

struct stats
{
    int preyCount;
    int predatorCount;
};



class MyTcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit MyTcpSocket(QObject *parent = 0);


    struct stats doConnect();
signals:

public slots:

private:
    QTcpSocket *socket;
    struct stats getStatServer(QTcpSocket *socket);
};

#endif // MYTCPSOCKET_H
