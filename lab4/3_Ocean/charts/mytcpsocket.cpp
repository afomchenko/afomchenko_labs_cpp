#include "mytcpsocket.h"
#include <QDateTime>
#include <QMessageBox>
#include <QString>


MyTcpSocket::MyTcpSocket(QObject *parent) :
    QObject(parent)
{
}

struct stats MyTcpSocket::doConnect()
     {

      struct stats statistics;
      statistics.predatorCount=1;
      statistics.preyCount=1;
        socket = new QTcpSocket(this);

        socket->connectToHost("localhost", 9999);

     if(socket->waitForConnected(5000))
     {

         socket->write("Hello Server\r\n");
         socket->waitForBytesWritten(1000);
         socket->waitForReadyRead(3000);

         statistics = getStatServer(socket);

         socket->close();

         return statistics;

     }
     //если не подключился возваращаем время по UTC
     return statistics;
     }



struct stats MyTcpSocket::getStatServer(QTcpSocket *socket)
{
    struct stats statistics;
    //входной поток
    QDataStream in(socket);

    //получаем время из потока
    in>>statistics.preyCount >>statistics.predatorCount;



    return statistics;
}


