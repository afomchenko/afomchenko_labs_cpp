#-------------------------------------------------
#
# Project created by QtCreator 2014-05-29T18:16:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = charts
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    nightcharts.cpp \
    mytcpsocket.cpp

HEADERS  += mainwindow.h \
    nightcharts.h \
    mytcpsocket.h

FORMS    += mainwindow.ui
