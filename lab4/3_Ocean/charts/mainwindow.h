#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "nightcharts.h"
#include <QTimer>
#include "mytcpsocket.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QTimer *timer;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    virtual void paintEvent(QPaintEvent* e);



    struct stats statistics;

private:
    Ui::MainWindow *ui;
    MyTcpSocket socket;

public slots:
        // void onUpdate();
};

#endif // MAINWINDOW_H
