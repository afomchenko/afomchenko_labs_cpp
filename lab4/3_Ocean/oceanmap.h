#ifndef OCEANMAP_H
#define OCEANMAP_H
#include "mainwindow.h"
#include <vector>
#include "cell.h"
#include "rock.h"
#include "prey.h"
#include "predator.h"

class Cell;
class Rock;
class Prey;
class Predator;

class OceanMap
{
public:
    OceanMap();

    int width;
    int height;

    std::vector < std::vector <Cell> > mapVector;

    std::vector <Rock> rockVector;
    std::vector <Prey> preyVector;
    std::vector <Predator> predVector;

    void addRock(const int,const  int);
    void addPrey(const int,const  int);
    void addPredator(const int,const  int);
    Rock rock;
    std::vector <std::vector <int> > locate(Cell &);


};

#endif // OCEANMAP_H
