#include "preythread.h"
#include <QTime>
#include <QMutex>


PreyThread::PreyThread(QObject *parent) :
    QThread(parent)
{
}

void PreyThread::run()
{
    if(mode==1) //для планктона
    while(1)
    {
        if(this->Stop)break;

        for(unsigned int i=0; i<ocean->preyVector.size();++i)
        {
            QMutexLocker locker(&seaMutex);
            if(ocean->preyVector.size()>0)
            {
                ocean->preyVector[i].turn();
                this->msleep(1);
            }
            else { Stop=true;  break;}
        }

    }
    else while(1) //для хищника
    {
        if(this->Stop)break;

        for(unsigned int i=0; i<ocean->predVector.size();++i)
        {
            QMutexLocker locker(&seaMutex);;
            if(ocean->predVector.size()>0)
            {
                ocean->predVector[i].turn();
                this->msleep(1);
            }
            else { Stop=true;  break;}
        }

    }

}

void PreyThread::updMapSlot()
{
    emit updMap();
}

