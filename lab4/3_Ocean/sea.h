#ifndef SEA_H
#define SEA_H

#include <QString>

class Sea
{
public:
    Sea();
    virtual void turn() = 0;
    virtual int getX() = 0;
    virtual int getY() = 0;
    virtual ~Sea();

};

#endif // SEA_H
