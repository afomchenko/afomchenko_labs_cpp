#ifndef CELL_H
#define CELL_H
#include "sea.h"


class Cell
{
public:
    Cell();
    Cell(int);

    Sea * object;

    int preyPriorityRep;
    int preyPrioritySafe;
    int predpriority;

    int food;
};

#endif // CELL_H
