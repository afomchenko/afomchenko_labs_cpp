#include "mytcpserver.h"
#include "oceanmap.h"

MyTcpServer::MyTcpServer(QObject *parent) :
    QObject(parent)
{
    server = new QTcpServer(this);

    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));

    if(!server->listen(QHostAddress::Any,9999))
    {
        qDebug() << "Server could not start!";
    }
    else
    {
        qDebug() << "Server started";
    }
}

void MyTcpServer::newConnection()
{
    QTcpSocket *socket = server->nextPendingConnection();

    sendStats(socket);
    socket->flush();

    socket->waitForBytesWritten(3000);

    socket->close();
}


void MyTcpServer::sendStats(QTcpSocket *socket)
{
    //блок данных
    QByteArray block;

    //поток данных на выход
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);

    out << static_cast<int>(ocean->preyVector.size()) << static_cast<int>(ocean->predVector.size());

    //пишем болк в сокет
    socket->write(block);
}
