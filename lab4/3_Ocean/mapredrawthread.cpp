#include "mapredrawthread.h"
#include <typeinfo>


MapReDrawThread::MapReDrawThread(QObject *parent) :
    QThread(parent)
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(onUpdate()));

}

void MapReDrawThread::run()
{
    timer->start(100);
}

void MapReDrawThread::onUpdate()
{
    const int PIX_SIZE = 5;

    scene->clear();scene->addRect(0,0,1200,650);

    QBrush greyBrush(Qt::darkGray);
    QBrush greenBrush(Qt::yellow);
    QBrush redBrush(Qt::darkRed);
    QPen greyPen(Qt::darkGray);
    QPen greenPen(Qt::yellow);
    QPen redPen(Qt::darkRed);

    for(int i=0; i<ocean->width; ++i)
    {
        for(int j=0; j< ocean->height; ++j)
        {
            if(ocean->mapVector[i][j].object!=NULL && typeid(*(ocean->mapVector[i][j].object))==typeid(Prey))
            {
                scene->addRect(i*PIX_SIZE-1, j*PIX_SIZE-1, PIX_SIZE, PIX_SIZE, greenPen, greenBrush);
            }
            else if(ocean->mapVector[i][j].object!=NULL && typeid(*(ocean->mapVector[i][j].object))==typeid(Predator))
            {
                scene->addRect(i*PIX_SIZE-1, j*PIX_SIZE-1, PIX_SIZE, PIX_SIZE, redPen, redBrush);

            }
           /* else if(ocean->mapVector[i][j].object!=NULL && typeid(*(ocean->mapVector[i][j].object))==typeid(Rock))
            {
                scene->addRect(i*5-1, j*5-1, 5, 5, greyPen, greyBrush);

            }*/
            else scene->addRect(i*PIX_SIZE-1, j*PIX_SIZE-1, PIX_SIZE, PIX_SIZE, QPen(( QColor(50,160+1*ocean->mapVector[i][j].food,200))), QBrush ( QColor(50,160+1*ocean->mapVector[i][j].food,200)) );;
        }
    }



    statbar->showMessage(QString::number(ocean->preyVector.size())+"   "+QString::number(ocean->predVector.size()));

}

MapReDrawThread::~MapReDrawThread()
{
    delete timer;
}

