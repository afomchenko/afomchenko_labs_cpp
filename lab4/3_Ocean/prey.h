#ifndef PREY_H
#define PREY_H
#include <QString>
#include "rock.h"
#include "oceanmap.h"

class OceanMap;

class Prey : virtual public Rock
{


public:
    static int count;

    int id;


    Prey();
    Prey(const int, const int , OceanMap * const);
    Prey(const Prey &);

    virtual void turn ();

    virtual void move (const int, const int);
    virtual void move (const int);

    virtual void reproduce(std::vector<std::vector<int> > );
    virtual std::vector<std::vector<int> > locateRep();

    virtual void die();
    virtual void feed ();

    int health;     //здоровье
    int satiety;    //сытость
    int age;        //возраст
    int fertility;  //плодовитость
    bool wounded;   //ранен

    OceanMap * ocean;
};



#endif // PREY_H
