#ifndef PREYTHREAD_H
#define PREYTHREAD_H

#include <QThread>
#include "mainwindow.h"
#include "oceanmap.h"
#include <QMutex>

class OceanMap;

class PreyThread : public QThread
{
    friend class MainWindow;

    Q_OBJECT
public:
    explicit PreyThread(QObject *parent = 0);
    void run();
    bool Stop;
    QMutex seaMutex;
    int mode;

    OceanMap * ocean;

signals:
    void posChanged(int oldx, int oldy, int x, int y, int i);
    void updMap();

public slots:
    void updMapSlot();

};

#endif // PREYTHREAD_H
