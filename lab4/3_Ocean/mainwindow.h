#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "oceanmap.h"
#include "preythread.h"
#include <vector>
#include <QtGui>
#include <QGraphicsRectItem>
#include <QTime>
#include "mapredrawthread.h"
#include "oceanredrawthread.h"
#include"mytcpserver.h"

class OceanMap;
class PreyThread;
class MapReDrawThread;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    OceanMap *ocean;
    QGraphicsScene *scene;

    PreyThread * preyThread;
    PreyThread * predThread;
    MapReDrawThread * mapredrawThread;

    QTimer *timer;
    MyTcpServer * myServer;

private slots:
    void on_actStart_triggered();

    void on_actRedraw_triggered();
    void onUpdate();

    void on_actStop_triggered();

    void on_zoomin_clicked();

    void on_zoomout_clicked();

private:
    Ui::MainWindow *ui;
    void redraw();
};

#endif // MAINWINDOW_H


