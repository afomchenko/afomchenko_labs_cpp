#-------------------------------------------------
#
# Project created by QtCreator 2014-05-20T18:52:18
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 3_OceanMod
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp \
    oceanmap.cpp \
    rock.cpp \
    sea.cpp \
    prey.cpp \
    preythread.cpp \
    predator.cpp \
    mapredrawthread.cpp \
    mytcpserver.cpp

HEADERS  += mainwindow.h \
    cell.h \
    oceanmap.h \
    rock.h \
    sea.h \
    prey.h \
    preythread.h \
    predator.h \
    mapredrawthread.h \
    mytcpserver.h

FORMS    += mainwindow.ui
