#ifndef MAPREDRAWTHREAD_H
#define MAPREDRAWTHREAD_H

#include <QThread>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QStatusBar>
#include "mainwindow.h"
#include "oceanmap.h"
#include "mytcpserver.h"

class MainWindow;
class OceanMap;
class MyTcpServer;

class MapReDrawThread : public QThread
{
    friend class MainWindow;
    Q_OBJECT
public:
    explicit MapReDrawThread(QObject *parent = 0);
    ~MapReDrawThread();
    void run();
    bool Stop;

    OceanMap * ocean;
    QGraphicsScene * scene;
    QTimer *timer;
    QStatusBar * statbar;
    MyTcpServer * myServer;



signals:


public slots:
     void onUpdate();
};

#endif // DRAWTHREAD_H
