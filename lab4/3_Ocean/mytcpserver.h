#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QDebug>
#include <QTcpServer>
#include <QTcpSocket>

#include <QDateTime>
#include "oceanmap.h"

class MyTcpServer : public QObject
{
    Q_OBJECT
public:
    explicit MyTcpServer(QObject *parent = 0);
     void sendStats(QTcpSocket *socket);
     OceanMap * ocean;
signals:

public slots:
    void newConnection();

private:
    QTcpServer *server;


};

#endif // MYTCPSERVER_H
