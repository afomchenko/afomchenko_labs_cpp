#include "predator.h"
#include "prey.h"
#include <typeinfo>

int Predator::count = 0;

Predator::Predator()
{
    count++;
    id=count;

    type="Predator";
    x=0;
    y=0;
    age=0;
    fertility=0;
    health=100;
    satiety=100;

}

Predator::Predator(const int  _x, const int  _y, OceanMap * const _ocean):Rock()
{
    count++;


    type="Predator";
    x=_x;
    y=_y;
    age=0;
    fertility=0;
    health=100;
    satiety=100;
    ocean=_ocean;
    id=count;
}


Predator::Predator(const Predator & _other):Rock()
{
    type=_other.type;
    x=_other.x;
    y=_other.y;
    ocean=_other.ocean;

    age=_other.age;
    fertility=_other.fertility;
    health=_other.health;
    satiety=_other.satiety;
    id=_other.id;
    ocean->mapVector[x][y].object=this;
}

//ход
void Predator::turn()
{
    fertility++;
    age++;
    satiety-=10;
     if((satiety<= 0 || age>1500 || health<1) && qrand()%2)
        die();
    else if (satiety>=80 && fertility>10)
        reproduce( locateRep());
    else if (satiety>40) move(7);
    else hunt( locatePrey());

}

//перемещаться в координаты
void Predator::move(const int _x, const int _y)
{
    int xold=x, yold=y;
    x+=_x-3;
    y+=_y-3;
    if(x<0) x=ocean->width+x;
    else if (x>=ocean->width) x=x-ocean->width;
    if(y<0) y=ocean->height+y;
    else if (y>=ocean->height) y=y-ocean->height;
    ocean->mapVector[x][y].object=this;
    ocean->mapVector[xold][yold].object=NULL;
}

//перемещаться по радиусу
void Predator::move(const int range)
{

    int priorMin =0;
    int _x, _y;
    int xnew, ynew;

    for(int i=0;i<range;++i)
    {
        for(int j=0;j<range;++j)
        {
            _x=x+i-range/2;
            if(_x<0) _x=ocean->width+_x;
            else if (_x>=ocean->width) _x=_x-ocean->width;
            _y=y+j-range/2;
            if(_y<0) _y=ocean->height+_y;
            else if (_y>=ocean->height) _y=_y-ocean->height;

            if(ocean->mapVector[_x][_y].object==NULL && priorMin<ocean->mapVector[_x][_y].food )
               {priorMin=ocean->mapVector[_x][_y].food; xnew=i; ynew=j;}
        }
    }

    if(xnew>1000 || ynew>1000 || xnew<0 || ynew<0) {xnew=0;ynew=0;}

    int xold=x, yold=y;
    x+=xnew-range/2;
    y+=ynew-range/2;
    if(x<0) x=ocean->width+x;
    else if (x>=ocean->width) x=x-ocean->width;
    if(y<0) y=ocean->height+y;
    else if (y>=ocean->height) y=y-ocean->height;

    ocean->mapVector[x][y].object=this;
    ocean->mapVector[xold][yold].object=NULL;
}


//искать партнера
std::vector <std::vector <int> > Predator::locateRep()
{
    std::vector <std::vector <int> > turnVec;
    std::vector <int> tmpIntVec;

    for(int i=0;i<7;++i)
    {
        for(int j=0;j<7;++j)
        {
            int _x=x+i-3;
            if(_x<0) _x=ocean->width+_x;
            else if (_x>=ocean->width) _x=_x-ocean->width;
            int _y=y+i-3;
            if(_y<0) _y=ocean->height+_y;
            else if (_y>=ocean->height) _y=_y-ocean->height;

            if(ocean->mapVector[_x][_y].object!=NULL && typeid(*(ocean->mapVector[_x][_y].object))==typeid(Predator))
               tmpIntVec.push_back(1);
            else
               tmpIntVec.push_back(0);
        }
        turnVec.push_back(tmpIntVec);
        tmpIntVec.clear();
    }
    return turnVec;
}

//размножаться
void Predator::reproduce(std::vector <std::vector <int> >  priorVec)
{
    int priorMin =0;
    int _x, _y;


    for(int i=0;i<7;++i)
    {
        for(int j=0;j<7;++j)
        {
            if(priorVec[i][j]>priorMin) {_x=i; _y=j; priorMin=priorVec[i][j];}
        }
    }
    if(_x<0 || _y<0) {_x=0;_y=0;}

    if(priorMin!=0)
    {
    int xold=x, yold=y;
    x+=_x-3;
    y+=_y-3+1;
    if(x<0) x=ocean->width+x;
    else if (x>=ocean->width) x=x-ocean->width;
    if(y<0) y=ocean->height+y;
    else if (y>=ocean->height) y=y-ocean->height;
    ocean->mapVector[x][y].object=this;
    ocean->mapVector[xold][yold].object=NULL;


    int xnew, ynew;
    xnew=x;
    ynew=y-2;
    if(xnew<0) xnew=ocean->width+xnew;
    else if (xnew>=ocean->width) xnew=xnew-ocean->width;
    if(ynew<0) ynew=ocean->height+ynew;
    else if (ynew>=ocean->height) ynew=ynew-ocean->height;
    ocean->addPredator(xnew,ynew);

    //сброс продуктивности
    fertility=0;
    }
}

//искать добычу
std::vector <std::vector <int> > Predator::locatePrey()
{
    std::vector <std::vector <int> > turnVec;
    std::vector <int> tmpIntVec;

    //матрица перемещения 7x7
    for(int i=0;i<7;++i)
    {
        for(int j=0;j<7;++j)
        {
            int _x=x+i-3;
            if(_x<0) _x=ocean->width+_x;
            else if (_x>=ocean->width) _x=_x-ocean->width;
            int _y=y+j-3;
            if(_y<0) _y=ocean->height+_y;
            else if (_y>=ocean->height) _y=_y-ocean->height;

            if(ocean->mapVector[_x][_y].object!=NULL && typeid(*(ocean->mapVector[_x][_y].object))==typeid(Prey))
               tmpIntVec.push_back(1);
            else
               tmpIntVec.push_back(0);
        }
        turnVec.push_back(tmpIntVec);
        tmpIntVec.clear();
    }
    return turnVec;
}

//охотиться
void Predator::hunt(std::vector <std::vector <int> >  priorVec)
{
    int priorMin =0;
    int _x, _y;

    //матрица перемещения 7x7
    for(int i=0;i<7;++i)
    {
        for(int j=0;j<7;++j)
        {
            if(priorVec[i][j]>priorMin) {_x=i; _y=j; priorMin=priorVec[i][j];}
        }
    }
    if(_x<0 || _y<0) {_x=0;_y=0;}

    if(priorMin!=0)
    {
    int xold=x, yold=y;
    x+=_x-3;
    y+=_y-3;
    if(x<0) x=ocean->width+x;
    else if (x>=ocean->width) x=x-ocean->width;
    if(y<0) y=ocean->height+y;
    else if (y>=ocean->height) y=y-ocean->height;

    kill(x,y);

    satiety=100;

    ocean->mapVector[x][y].object=this;
    ocean->mapVector[xold][yold].object=NULL;

    }
}


//умереть
void Predator::die()
{
    ocean->mapVector[x][y].object=NULL;
    for(unsigned int i=0;i<ocean->predVector.size();++i)
        if (ocean->predVector[i].id==this->id) ocean->predVector.erase(ocean->predVector.begin() + i);

}


//убить добычу
void Predator::kill(const int _x, const int _y)
{
    for(unsigned int i=0;i<ocean->preyVector.size();++i)
       if (ocean->preyVector[i].getX()==_x && ocean->preyVector[i].getY()==_y) ocean->preyVector[i].wounded=true;

}

//покормиться
void Predator::feed()
{
    satiety+=ocean->mapVector[x][y].food;
    ocean->mapVector[x][y].food-=10;
    if(ocean->mapVector[x][y].food<0) ocean->mapVector[x][y].food=0;
    satiety=satiety>100?100:satiety;
}
