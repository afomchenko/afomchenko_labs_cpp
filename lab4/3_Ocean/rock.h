#ifndef ROCK_H
#define ROCK_H
#include <QString>
#include "sea.h"

class Rock : virtual public Sea
{
public:
    Rock();
    Rock(const int, const int);
    Rock(const Rock &);
    QString type;

    virtual void turn ();
    virtual int getX();
    virtual int getY();

protected:
    int x;
    int y;
};

#endif // ROCK_H
