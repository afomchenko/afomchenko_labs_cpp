#ifndef PREDATOR_H
#define PREDATOR_H

#include <QString>
#include "rock.h"
#include "oceanmap.h"

class Predator : virtual public Rock
{


public:
    static int count;

    int id;

    Predator();
    Predator(const int, const int , OceanMap * const);
    Predator(const Predator &);

    virtual void turn ();

    virtual void move (const int, const int);
    virtual void move (const int);

    virtual void reproduce(std::vector<std::vector<int> > );
    virtual std::vector<std::vector<int> > locateRep();

    virtual void die();
    virtual void feed ();

    virtual std::vector<std::vector<int> > locatePrey();

    virtual void hunt(std::vector<std::vector<int> > );

    virtual void kill(const int, const int);

    int health;     //здоровье
    int satiety;    //сытость
    int age;        //возраст
    int fertility;  //плодовитость

    OceanMap * ocean;

};

#endif // PREDATOR_H
