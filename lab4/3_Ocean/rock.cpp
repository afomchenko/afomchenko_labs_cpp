#include "rock.h"

Rock::Rock()
{
    type="rock";
}

Rock::Rock(const int _x, const int _y)
{
    type="rock";
    x=_x;
    y=_y;
}

Rock::Rock(const Rock & _other)
{
    type=_other.type;
    x=_other.x;
    y=_other.y;
}


int Rock::getX()
{
    return x;
}

int Rock::getY()
{
    return y;
}

void Rock::turn()
{
}
