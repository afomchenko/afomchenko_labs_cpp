#ifndef OCEANREDRAWTHREAD_H
#define OCEANREDRAWTHREAD_H

#include <QThread>

class OceanRedrawThread : public QThread
{
    Q_OBJECT
public:
    explicit OceanRedrawThread(QObject *parent = 0);
    void run();

signals:

public slots:

};

#endif // OCEANREDRAWTHREAD_H
