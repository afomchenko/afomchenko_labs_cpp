#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QString>
#include <QListWidgetItem>


#include "Automata.h"
#include "Currency.h"
#include "MyStr.h"

    static Automata CoffeeMan;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    CoffeeMan.setON(ui);

    imageShow(":/img/empty.jpg");

   loadMenuList(CoffeeMan);

   ui->txtMessage->appendPlainText("Выберите напиток в меню...");
   ui->txtMessage->moveCursor (QTextCursor::Down) ;
   ui->txtMessage->ensureCursorVisible();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadMenuList(const Automata & CoffeeMan)
{
     QListWidgetItem *item=new QListWidgetItem[CoffeeMan.menuSize];

    for(int i=1; i<CoffeeMan.menuSize; ++i)
    {
        ui->menu->addItem(&item[i]);
        item[i].setText((QString)CoffeeMan.menu[i].getStr()+ "\t\t"  +QString::number(CoffeeMan.prices[i].getRub()) + " руб. " +(CoffeeMan.prices[i].getKop()!=0 ? QString::number(CoffeeMan.prices[i].getKop())+ " коп.":" "));
    }

    ui->menu->setCurrentRow(0);
}


void MainWindow::on_btnMoney50p_clicked()
{
    imageShow(":/img/empty.jpg");

    if(CoffeeMan.getCashRub()<950){
        CoffeeMan.addToCash(50, 0);
        ui->lcdRub->display(CoffeeMan.getCashRub());
        ui->lcdKop->display(CoffeeMan.getCashKop());
        ui->txtMessage->appendPlainText("добавлено 50 рублей");
    }
    else   ui->txtMessage->appendPlainText("ошибка: слишком много денег");

}

void MainWindow::on_btnMoney10p_clicked()
{
    imageShow(":/img/empty.jpg");

    if(CoffeeMan.getCashRub()<990){
        CoffeeMan.addToCash(10, 0);
        ui->lcdRub->display(CoffeeMan.getCashRub());
        ui->lcdKop->display(CoffeeMan.getCashKop());
        ui->txtMessage->appendPlainText("добавлено 10 рублей");
    }
    else   ui->txtMessage->appendPlainText("ошибка: слишком много денег");
}

void MainWindow::on_btnMoney5p_clicked()
{
    imageShow(":/img/empty.jpg");

    if(CoffeeMan.getCashRub()<995){
        CoffeeMan.addToCash(5, 0);
        ui->lcdRub->display(CoffeeMan.getCashRub());
        ui->lcdKop->display(CoffeeMan.getCashKop());
        ui->txtMessage->appendPlainText("добавлено 5 рублей");
    }
    else   ui->txtMessage->appendPlainText("ошибка: слишком много денег");
}

void MainWindow::on_btnMoney1p_clicked()
{
    imageShow(":/img/empty.jpg");

    if(CoffeeMan.getCashRub()<999){
        CoffeeMan.addToCash(1, 0);
        ui->lcdRub->display(CoffeeMan.getCashRub());
        ui->lcdKop->display(CoffeeMan.getCashKop());
        ui->txtMessage->appendPlainText("добавлен 1 рубль");
    }
    else   ui->txtMessage->appendPlainText("ошибка: слишком много денег");
}

void MainWindow::on_btnMoney50k_clicked()
{
    imageShow(":/img/empty.jpg");

    if(CoffeeMan.getCashRub()>=999 && CoffeeMan.getCashKop()>=50 )
        ui->txtMessage->appendPlainText("ошибка: слишком много денег");
    else
    {
        CoffeeMan.addToCash(0, 50);
        ui->lcdRub->display(CoffeeMan.getCashRub());
        ui->lcdKop->display(CoffeeMan.getCashKop());
        ui->txtMessage->appendPlainText("добавлено 50 копеек");
    }
}

void MainWindow::on_btnMoney10k_clicked()
{
    imageShow(":/img/empty.jpg");

    if(CoffeeMan.getCashRub()>=999 && CoffeeMan.getCashKop()>=90 )
        ui->txtMessage->appendPlainText("ошибка: слишком много денег");
    else
    {
        CoffeeMan.addToCash(0, 10);
        ui->lcdRub->display(CoffeeMan.getCashRub());
        ui->lcdKop->display(CoffeeMan.getCashKop());
        ui->txtMessage->appendPlainText("добавлено 50 копеек");
    }
}

void MainWindow::on_btnChoice_clicked()
{
   //ui->txtMessage->appendPlainText(QString::number(ui->menu->currentRow()));

   // ui->txtMessage->appendPlainText((int)ui->checkSugar->isChecked());

    if(ui->menu->currentRow()<0 || ui->menu->currentRow()+1>CoffeeMan.menuSize)
    {
        ui->txtMessage->appendPlainText("не выбран напиток");
    }
    else if(CoffeeMan.choice(ui->menu->currentRow()+1,(int) ui->checkSugar->isChecked()))
    {
        ui->txtMessage->appendPlainText("подождите, готовится...");

        if(CoffeeMan.cooking(ui->menu->currentRow()+1,(int) ui->checkSugar->isChecked())==0)
        {
            if(ui->menu->currentRow()<4)
            imageShow(":/img/coffee.jpg");
            else if(ui->menu->currentRow()==4 || ui->menu->currentRow()==5 )
            imageShow(":/img/tea.jpg");
            else if(ui->menu->currentRow()==6)
            imageShow(":/img/coffee.jpg");
            else if(ui->menu->currentRow()==7)
            imageShow(":/img/water.jpg");

            ui->txtMessage->appendPlainText("напиток готов");
            on_btnCancel_clicked();
        }
        else
        {
            switch (CoffeeMan.cooking(ui->menu->currentRow()+1,(int) ui->checkSugar->isChecked()))
            {
                case 2: ui->txtMessage->appendPlainText("ошибка: закончилась вода"); break;
                case 3: ui->txtMessage->appendPlainText("ошибка: закончилось кофе"); break;
                case 4: ui->txtMessage->appendPlainText("ошибка: закончилось молоко"); break;
                case 5: ui->txtMessage->appendPlainText("ошибка: закончился чай"); break;
                case 6: ui->txtMessage->appendPlainText("ошибка: закончилось какао"); break;
                case 7: ui->txtMessage->appendPlainText("ошибка: закончился сахар"); break;
                default: ui->txtMessage->appendPlainText("ошибка: неисправно");
            }
        }
    }
    else
    {
        ui->txtMessage->appendPlainText("ошибка: недостаточно средств");
        if(ui->checkSugar->isChecked()) ui->txtMessage->appendPlainText("сахар стоит 1 р.");
    }

}

void MainWindow::on_checkSugar_clicked()
{
        if(ui->checkSugar->isChecked()) ui->txtMessage->appendPlainText("сахар стоит 1 р.");
}

void MainWindow::on_btnCancel_clicked()
{
    QString outChangeText = "Не забудьте сдачу: " + QString::number(CoffeeMan.getCashRub()) + "руб." +QString::number(CoffeeMan.getCashKop()) + "коп."  ;

    ui->txtMessage->appendPlainText(outChangeText);

    CoffeeMan.finish();

    ui->lcdRub->display(CoffeeMan.getCashRub());
    ui->lcdKop->display(CoffeeMan.getCashKop());
}

void MainWindow::imageShow(const QString & fileName)
{
    QPixmap pixmap(fileName);
    ui->imageLabel->setPixmap(pixmap);
    ui->imageLabel->setMask(pixmap.mask());

    ui->imageLabel->show();
}
