#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "Automata.h"

class Automata;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void loadMenuList(const Automata & CoffeeMan);
    void imageShow(const QString &);


private slots:
    void on_btnMoney50p_clicked();

    void on_btnMoney10p_clicked();

    void on_btnMoney5p_clicked();

    void on_btnMoney1p_clicked();

    void on_btnMoney50k_clicked();

    void on_btnMoney10k_clicked();

    void on_btnChoice_clicked();

    void on_checkSugar_clicked();

    void on_btnCancel_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
