﻿#ifndef AUTOMATA_H
#define AUTOMATA_H

#include "Currency.h"
#include "MyStr.h"

#include "mainwindow.h"
#include <QMainWindow>
#include "ui_mainwindow.h"


using namespace std;


class Automata
{
   friend class MainWindow;

public:
	Automata();
    Automata(const Automata &);
	~Automata();

	void setON();
    void setON(Ui::MainWindow * ui);

    void setOFF();

	void operate();

    int getCashRub()const {return cash.getRub();}
    int getCashKop()const {return cash.getKop();}


	void loadMenu(const MyStr &, const Currency &, const int&, const int&, const int&, const int&, const int&, const int& _sugar=0);
    void addToCash(const int, const int =0);
    bool choice(const int, const bool);
    int cooking(const int, const bool);
	void finish();

    MyStr * menu;
    Currency * prices;

private:
	Currency cash;

	int menuSize;


	int * water;
	int * coffee;
	int * milk;
	int * cacao;
	int * tea;
	int * sugar;

	int waterFull;
	int coffeeFull;
	int milkFull;
	int cacaoFull;
	int teaFull;
	int sugarFull;

	enum mode {
		off,	//0
		on,		//1
		wait,	//2
		accept,	//3
		check,	//4
		cook	//5
	};
	mode status;

};




#endif
