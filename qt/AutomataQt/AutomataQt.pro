#-------------------------------------------------
#
# Project created by QtCreator 2014-03-30T00:52:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AutomataQt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Automata.cpp \
    Currency.cpp \
    MyStr.cpp

HEADERS  += mainwindow.h \
    Automata.h \
    Currency.h \
    MyStr.h

FORMS    += mainwindow.ui

OTHER_FILES +=

RESOURCES += \
    img.qrc
