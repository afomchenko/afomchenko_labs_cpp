﻿#include "Automata.h"
#include "Currency.h"
#include "MyStr.h"

#include <cstdlib>
#include <iostream>

#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

const int maxMenuSize = 16;




Automata::Automata()
{


	Currency cash;
	menu = new MyStr[maxMenuSize];
	menu[0]="cancel";
	menuSize = 1;
	prices = new Currency[maxMenuSize];
	status = off;

	water = new int[maxMenuSize];
	coffee = new int[maxMenuSize];
	milk = new int[maxMenuSize];
	cacao = new int[maxMenuSize];
	tea = new int[maxMenuSize];
	sugar = new int[maxMenuSize];

	waterFull = 100;
	coffeeFull = 100;
	milkFull = 100;
	cacaoFull = 100;
	teaFull = 100;
	sugarFull = 100;
}

Automata::Automata(const Automata & _other)
{


    Currency cash;
    cash=_other.cash;
    menu = new MyStr[maxMenuSize];
    memcpy(menu,_other.menu,maxMenuSize);
    menuSize = _other.menuSize;
    prices = new Currency[maxMenuSize];
    memcpy(prices,_other.prices,maxMenuSize);

    status = _other.status;


    water = new int[maxMenuSize];
    coffee = new int[maxMenuSize];
    milk = new int[maxMenuSize];
    cacao = new int[maxMenuSize];
    tea = new int[maxMenuSize];
    sugar = new int[maxMenuSize];

    memcpy(water,_other.water,maxMenuSize);
    memcpy(coffee,_other.coffee,maxMenuSize);
    memcpy(milk,_other.milk,maxMenuSize);
    memcpy(cacao,_other.cacao,maxMenuSize);
    memcpy(tea,_other.tea,maxMenuSize);
    memcpy(sugar,_other.sugar,maxMenuSize);

    waterFull = _other.waterFull;
    coffeeFull = _other.coffeeFull;
    milkFull = _other.milkFull;
    cacaoFull = _other.cacaoFull;
    teaFull = _other.teaFull;
    sugarFull = _other.sugarFull;
}

Automata::~Automata()
{
	delete[] menu;
	delete[] prices;
}

void Automata::setON(Ui::MainWindow * ui)
{
	status = on;

    ui->txtMessage->setPlainText("Microsoft CoffeeMan 2014 ver 1.0 beta");
    ui->txtMessage->appendPlainText("check water...\t OK");
    ui->txtMessage->appendPlainText("load menu...\t OK");


    loadMenu(" Эспрессо    ", Currency(15, 50), 10, 10, 0, 0, 0);
    loadMenu(" Латте       ", Currency(22, 80), 10, 10, 20, 0, 0);
    loadMenu(" Каппучино   ", Currency(20, 30), 10, 10, 10, 0, 0);
    loadMenu(" Американо   ", Currency(16, 50), 20, 10, 10, 0, 0);
    loadMenu(" Чай черный  ", Currency(13, 20), 20, 0, 0, 10, 0);
    loadMenu(" Чай зеленый ", Currency(14, 00), 20, 0, 0, 10, 0);
    loadMenu(" Гор. шоколад", Currency(24, 00), 0, 0, 20, 0, 10, 5);
    loadMenu(" Кипяточек   ", Currency(5, 00), 20, 0, 0, 0, 0);

    ui->txtMessage->appendPlainText("check coffee...\t OK");
    ui->txtMessage->moveCursor (QTextCursor::Down) ;
    ui->txtMessage->ensureCursorVisible();
    ui->txtMessage->appendPlainText("check milk...\t OK");
    ui->txtMessage->moveCursor (QTextCursor::Down) ;
    ui->txtMessage->ensureCursorVisible();



	status = wait;
    //operate();
}


void Automata::setOFF()
{
	if (status != off)
	{
	cout << "shut down..." << endl;
	status = off;
	}
}

void Automata::loadMenu(const MyStr & _item, const Currency & _cost, const int& _water, const int& _coffee, const int& _milk, const int& _tea, const int& _cacao, const int& _sugar)
{
	if (menuSize < maxMenuSize)
	{

		menu[menuSize] = _item;
		prices[menuSize] = _cost;
		//сколько надо ингредиентов?
		water[menuSize] = _water;
		coffee[menuSize] = _coffee;
		milk[menuSize] = _milk;
		tea[menuSize] = _tea;
		cacao[menuSize] = _cacao;
		sugar[menuSize] = _sugar;

		menuSize++;
	}

	else cout << "error: menu full!!!" << endl;
}


void Automata::addToCash(const int rub, const int kop)
{
	status = check;
	Currency addCoin;

    addCoin.setRub(rub);
    addCoin.setKop(kop);
	
	cash += addCoin;
	status=accept;
}



bool Automata::choice(const int menuNo, const bool sugarChoice)
{
	status = check;
	Currency sugarCost(sugarChoice);
	if (prices[menuNo] + sugarCost  > cash)
	{
        return 0;
	}
    else return 1;
}

int Automata::cooking(const int menuNo, const bool sugarChoice )
{
	status = cook;

	//прверяем ингредиенты
	int empty = 0;
    if (water[menuNo] > waterFull)		return 2;
    if (coffee[menuNo] > coffeeFull)	return 3;
    if (milk[menuNo] > milkFull)		return 4;
    if (tea[menuNo] > teaFull)			return 5;
    if (cacao[menuNo] > cacaoFull)		return 6;
    if (sugar[menuNo] > sugarFull)		return 7;
	if (empty > 0) finish();

	//забираем ингредиенты
	waterFull -= water[menuNo];
	coffeeFull -= coffee[menuNo];
	milkFull -= milk[menuNo];
	teaFull -= tea[menuNo];
	cacaoFull -= cacao[menuNo];
	sugarFull -= sugar[menuNo]+(static_cast<int>(sugarChoice)*5);

	//сдача
	cash -= prices[menuNo]+static_cast<int>(sugarChoice);


    status = on;
    return 0;

}

void Automata::finish()
{
	status = on;
	cash.setRub(0); cash.setKop(0);
    //if (getOFF=='Y' || getOFF=='y' || getOFF=='1') status=wait;
    //else setOFF();


}


