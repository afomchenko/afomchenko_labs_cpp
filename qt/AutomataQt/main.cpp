#include "mainwindow.h"
#include <QApplication>

#include "Automata.h"
#include "Currency.h"
#include "MyStr.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    //Automata CoffeeMan;

    //CoffeeMan.setON();

    return a.exec();
}
