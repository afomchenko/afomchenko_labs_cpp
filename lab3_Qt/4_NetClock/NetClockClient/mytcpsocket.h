#ifndef MYTCPSOCKET_H
#define MYTCPSOCKET_H

#include <QObject>
#include <QDebug>
#include <QTcpSocket>

class MyTcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit MyTcpSocket(QObject *parent = 0);
    QDateTime doConnect();
signals:

public slots:

private:
    QTcpSocket *socket;
    QDateTime getDateTimeServer(QTcpSocket *socket);
};

#endif // MYTCPSOCKET_H
