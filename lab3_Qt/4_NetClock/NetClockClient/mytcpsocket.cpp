#include "mytcpsocket.h"
#include <QDateTime>

MyTcpSocket::MyTcpSocket(QObject *parent) :
    QObject(parent)
{
}

QDateTime MyTcpSocket::doConnect()
     {


        socket = new QTcpSocket(this);

        socket->connectToHost("localhost", 9999);

     if(socket->waitForConnected(5000))
     {

         socket->write("Hello Server\r\n");
         socket->waitForBytesWritten(1000);
         socket->waitForReadyRead(3000);

         QDateTime time = getDateTimeServer(socket);

         socket->close();

         return time;

     }
     //если не подключился возваращаем время по UTC
     return QDateTime::currentDateTimeUtc();
     }

QDateTime MyTcpSocket::getDateTimeServer(QTcpSocket *socket)
{
    QDateTime time;
    //входной поток
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_5_2);

    //получаем время из потока
    in>>time;

    return time;
}


