#-------------------------------------------------
#
# Project created by QtCreator 2014-04-10T18:27:17
#
#-------------------------------------------------

QT       += core network
QT       -= gui

TARGET = MyTcpClient
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mytcpserver.cpp

HEADERS += \
    mytcpserver.h
