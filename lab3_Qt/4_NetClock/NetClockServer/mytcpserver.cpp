#include "mytcpserver.h"
#include <QDateTime>

MyTcpServer::MyTcpServer(QObject *parent) :
    QObject(parent)
{

     server=new QTcpServer (this);
     connect(server,SIGNAL(newConnection()),this, SLOT(newConnecion()));
     if(!server->listen(QHostAddress::Any,9999))
     {
         qDebug()<<"Error!";
     }
                   else
     {
                   qDebug()<<"Server started!";
     }
}

void MyTcpServer::newConnecion()
     {
        QTcpSocket *socket = server->nextPendingConnection();


                   sendDateTime(socket);

                   socket->flush();
                   socket->waitForBytesWritten(3000);

                   socket->close();
     }


void MyTcpServer::sendDateTime(QTcpSocket *socket)
{
    //блок данных
    QByteArray block;

    //поток данных на выход
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_2);

    //пишем в поток
    out << QDateTime::currentDateTime();

    //пишем болк в сокет
    socket->write(block);
}
