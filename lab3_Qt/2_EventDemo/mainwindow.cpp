#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QDateTime>
#include <QTimer>
#include <cmath>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    eventListSize=0;

    //ui->addDateTime->setDateTime(QDateTime::currentDateTime());
    item=new QListWidgetItem[32];
    listDateTime  = new QDateTime[32];
    listEventName = new QString[32];

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updEventList()));
    timer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addEventButton_clicked()
{

    int diff= ui->addDateTime->dateTime().secsTo(QDateTime::currentDateTime());
    ui->eventList->addItem(&item[eventListSize]);
    listDateTime[eventListSize]=ui->addDateTime->dateTime();
    listEventName[eventListSize]=ui->addEventName->text();
    item[eventListSize].setText(listEventName[eventListSize]+(diff>0?" прошло: ":" осталось: ")
                    +(diff/3153600!=0 ? QString::number(abs(diff/3153600))+" лет. " : " ")
                    +(diff%3153600/86400!=0 ? QString::number(abs(diff%3153600/86400))+" дней. " : " ")
                    +(diff%86400/3600!=0 ? QString::number(abs(diff%86400/3600))+" часов. " : " ")
                    +(diff%3600/60!=0 ? QString::number(abs(diff%3600/60))+" минут. " : " ")
                    +(diff%60!=0 ? QString::number(abs(diff%60))+" секунд. " : " ")
                    );
    ui->addEventName->clear();
    ui->addDateTime->setDateTime(QDateTime::currentDateTime());
    eventListSize++;
}

void MainWindow::updEventList()
{
    for(int i=0; i<=eventListSize; ++i)
    {
            int diff= listDateTime[i].secsTo(QDateTime::currentDateTime());
            item[i].setText(listEventName[i]+(diff>0?" прошло: ":" осталось: ")
                            +(diff/31556926!=0 ? QString::number(abs(diff/31556926))+" лет. " : " ")
                            +(diff%31556926/86400!=0 ? QString::number(abs(diff%31556926/86400))+" дней. " : " ")
                            +(diff%86400/3600!=0 ? QString::number(abs(diff%86400/3600))+" часов. " : " ")
                            +(diff%3600/60!=0 ? QString::number(abs(diff%3600/60))+" минут. " : " ")
                            +(diff%60!=0 ? QString::number(abs(diff%60))+" секунд. " : " ")
                            );
    }
}
