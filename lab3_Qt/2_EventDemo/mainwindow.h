#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void loadEventList();
    void addEvent(QString &, QDateTime);

public slots:

private slots:
    void on_addEventButton_clicked();
    void updEventList();

private:

    Ui::MainWindow *ui;
    QListWidgetItem *item;
    QDateTime * listDateTime;
    QString * listEventName;
    int eventListSize;



};

#endif // MAINWINDOW_H
